import React from 'react';
import { SafeAreaView, StatusBar, StyleSheet } from 'react-native';
import GridView from 'react-native-flex-grid-view';

import Header from './components/Header';
import KeyBox from './components/KeyBox';
import LogBox from './components/LogBox';
import GlobalStyles from './assets/styles/GlobalStyles';

const App = (): JSX.Element => {
  return (
    <SafeAreaView style={[GlobalStyles().backgroundStyle, styles.wrapper]}>
      <StatusBar
        barStyle={GlobalStyles().barStyles}
        backgroundColor={GlobalStyles().backgroundStyle.backgroundColor}
      />
      <Header />
      <GridView
        data={['#1abc9c', '#3498db']}
        span={2}
        spacing={10}
        square
        flat={false}
        render={(item: any) => <KeyBox style={{ backgroundColor: item }} />}
      ></GridView>
      <GridView
        data={['#34495e']}
        span={1}
        flat={false}
        render={(item: any) => <LogBox style={{ backgroundColor: item }} />}
        style={[styles.logWrapper]}
      ></GridView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  logWrapper: {
    flex: 1,
    marginHorizontal: 10,
  },
});

export default App;
