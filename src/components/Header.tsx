import React, { useEffect, useRef, useState } from 'react';
import { StyleSheet, TouchableWithoutFeedback, View, Animated, Easing } from 'react-native';
import Lottie from 'lottie-react-native';
import {
  BIRD_START,
  BIRD_END,
  BIRD_DURATION,
  EXPLODING_START,
  EXPLODING_END,
  EXPLODING_DURATION,
} from '../assets/lottie';

const Header = () => {
  const [animationState, setAnimationState] = useState('bird');
  const birdProgress = useRef(new Animated.Value(BIRD_START));
  const explodingProgress = useRef(new Animated.Value(EXPLODING_START));
  const birdAnimation = Animated.timing(birdProgress.current, {
    toValue: BIRD_END,
    duration: BIRD_DURATION,
    easing: Easing.linear,
    useNativeDriver: false,
  });
  const explodingAnimation = Animated.timing(explodingProgress.current, {
    toValue: EXPLODING_END,
    duration: EXPLODING_DURATION,
    easing: Easing.linear,
    useNativeDriver: false,
  });

  useEffect(() => {
    Animated.loop(birdAnimation).start();
  }, []);

  return (
    <>
      <View style={[styles.wrapper, { display: animationState === 'bird' ? 'flex' : 'none' }]}>
        <TouchableWithoutFeedback
          onPress={function () {
            setAnimationState('exploding');
            explodingAnimation.start(() => {
              explodingAnimation.reset();
              setAnimationState('bird');
            });
          }}
        >
          <Lottie
            source={require('../assets/lottie/exploding-bird.json')}
            progress={birdProgress.current}
          />
        </TouchableWithoutFeedback>
      </View>
      <View style={[styles.wrapper, { display: animationState === 'exploding' ? 'flex' : 'none' }]}>
        <Lottie
          source={require('../assets/lottie/exploding-bird.json')}
          progress={explodingProgress.current}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    paddingBottom: 40,
    paddingTop: 96,
  },
});

export default Header;
