import React, { useEffect, useRef, useState } from 'react';
import { StyleSheet, TouchableWithoutFeedback, View, Text } from 'react-native';

const KeyBox = (props: any) => {
  return (
    <View style={[props.style, styles.wrapper]}>
      <TouchableWithoutFeedback onPress={function () {}}>
        <View style={styles.content}>
          <Text style={styles.text}>sdfsdf</Text>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    borderRadius: 10,
  },
  content: {
    margin: 16,
  },
  text: {
    color: '#fff',
  },
});

export default KeyBox;
