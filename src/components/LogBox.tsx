import React, { useEffect, useState } from 'react';
import { StyleSheet, View, ScrollView, Text } from 'react-native';
import { getDoorLog } from '../api/door';

const LogBox = (props: any) => {
  const [logList, setLogList] = useState([]);
  useEffect(() => {
    getDoorLog()
      .then((res) => {
        setLogList(res?.data?.items);
      })
      .catch((error) => {
        console.log('error', error);
      });
  }, []);

  return (
    <View style={[props.style, styles.wrapper]}>
      <ScrollView style={styles.content}>
        {logList.map((item: any) => (
          <View style={styles.contentItems} key={item.key}>
            <Text style={[styles.text, styles.date]}>{item.date}</Text>
            <Text style={[styles.text, styles.door]}>{item.door}</Text>
            <Text style={[styles.text, styles.reason]}> {item.reason}</Text>
          </View>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    width: '100%',
    height: '100%',
    borderRadius: 10,
  },
  content: {
    margin: 16,
  },
  contentItems: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  text: {
    color: '#fff',
  },
  date: {
    flex: 2,
  },
  door: {
    flex: 2,
  },
  reason: {
    flex: 3,
  },
});

export default LogBox;
