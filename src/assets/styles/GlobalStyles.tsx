import { StyleSheet, useColorScheme, StatusBarStyle } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';

const GlobalStyles = () => {
  const isDarkMode = useColorScheme() === 'dark';
  const themeStyles = isDarkMode ? darkTheme : lightTheme;
  const barStyles: StatusBarStyle = isDarkMode ? 'light-content' : 'dark-content';

  const styles = {
    ...commonStyles,
    ...themeStyles,
    barStyles,
  };

  return styles;
};

const commonStyles = StyleSheet.create({});

const darkTheme = StyleSheet.create({
  backgroundStyle: {
    backgroundColor: Colors.darker,
  },
});

const lightTheme = StyleSheet.create({
  backgroundStyle: {
    backgroundColor: Colors.lighter,
  },
});

export default GlobalStyles;
