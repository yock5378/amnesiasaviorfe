import req from './index';

export const getDoorLog = () => {
  return req('get', '/door/log');
};
