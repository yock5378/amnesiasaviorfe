import axios from 'axios';
import { API_URL, API_TOKEN } from '@env';

const axiosInstance = axios.create({
  baseURL: API_URL,
  headers: {
    'Content-Type': 'application/json',
    'X-API-Key': API_TOKEN,
  },
  timeout: 20000,
});

axiosInstance.interceptors.request.use(
  (config) => {
    return config;
  },
  (error) => {
    return Promise.reject(error);
  },
);

axiosInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.response) {
      switch (error.response.status) {
        case 404:
          console.log('頁面不存在');
          break;
        case 500:
          console.log('程式發生問題');
          break;
        default:
          console.log(error.message);
      }
    }
    return Promise.reject(error);
  },
);

export default (method: string, url: string, data: Object | null = null, config?: Object) => {
  method = method.toLowerCase();
  switch (method) {
    case 'post':
      return axiosInstance.post(url, data, config);
    case 'get':
      return axiosInstance.get(url, { params: data });
    case 'delete':
      return axiosInstance.delete(url, { params: data });
    case 'put':
      return axiosInstance.put(url, data);
    case 'patch':
      return axiosInstance.patch(url, data);
    default:
      return Promise.reject(`未知的 method: ${method}`);
  }
};
