## Getting Started

```bash
# run ios
yarn ios
```

## Ref.

[Setting up the development environment](https://reactnative.dev/docs/environment-setup)

[Clear the cache of pod](https://github.com/facebook/react-native/issues/26118)

[Workaround for Xcode 13.4 RC bug](https://github.com/travis-mark/lrn/commit/015854716feadd61a904d5a603b027426472f863#diff-281ded35b124f5160d0a57e47e521ee6f49f233933e091a0d33f3fd42a74abc8R25)
